<?php
/**
 * We Donuts - mu-plugin
 *
 * PHP version 7
 *
 * @category  Wordpress_Mu-plugin
 * @package   MarinaFlores
 * @author    Nextcom <contato@nextcom.digital>
 * @copyright 2019 Nextcom
 * @license   Proprietary https://nextcom.digital
 * @link      https://delivery.marinaflores.com.br
 *
 * @wordpress-plugin
 * Plugin Name: LP Delivery Marina Flores - mu-plugin
 * Plugin URI:  https://delivery.marinaflores.com.br
 * Description: Customizations for delivery.marinaflores.com.br site
 * Version:     1.0.0
 * Author:      Nextcom
 * Author URI:  https://nextcom.digital/
 * Text Domain: deliverymarinaflorescombr
 * License:     Proprietary
 * License URI: https://nextcom.digital
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Load Translation
 *
 * @return void
 */
add_action(
    'plugins_loaded',
    function () {
        load_muplugin_textdomain('deliverymarinaflorescombr', basename(dirname(__FILE__)).'/languages');
    }
);

/**
 * Hide editor from all pages
 */
add_action(
    'admin_init',
    function () {
        remove_post_type_support('page', 'editor');
    }
);

/***********************************************************************************
 * Callback Functions
 **********************************************************************************/

 /**
  * Metabox for Page Slug
  *
  * @param bool  $display  Display
  * @param array $meta_box Metabox 
  *
  * @return bool $display
  *
  * @author Tom Morton
  * @link   https://github.com/CMB2/CMB2/wiki/Adding-your-own-show_on-filters
  */
function Cmb2_Metabox_Show_On_slug($display, $meta_box)
{
    if (!isset($meta_box['show_on']['key'], $meta_box['show_on']['value'])) {
        return $display;
    }

    if ('slug' !== $meta_box['show_on']['key']) {
        return $display;
    }

    $post_id = 0;

    // If we're showing it based on ID, get the current ID
    if (isset($_GET['post'])) {
        $post_id = $_GET['post'];
    } elseif (isset($_POST['post_ID'])) {
        $post_id = $_POST['post_ID'];
    }

    if (!$post_id) {
        return $display;
    }

    $slug = get_post($post_id)->post_name;

    // See if there's a match
    return in_array($slug, (array) $meta_box['show_on']['value']);
}
add_filter('cmb2_show_on', 'Cmb2_Metabox_Show_On_slug', 10, 2);

/**
 * Gets a number of terms and displays them as options
 *
 * @param CMB2_Field $field CMB2 Field
 *
 * @return array An array of options that matches the CMB2 options array
 */
function Cmb2_getTermOptions($field)
{
    $args = $field->args('get_terms_args');
    $args = is_array($args) ? $args : array();

    $args = wp_parse_args($args, array('taxonomy' => 'category'));

    $taxonomy = $args['taxonomy'];

    $terms = (array) cmb2_utils()->wp_at_least('4.5.0')
        ? get_terms($args)
        : get_terms($taxonomy, $args);

    // Initate an empty array
    $term_options = array();
    if (!empty($terms)) {
        foreach ($terms as $term) {
            $term_options[ $term->term_id ] = $term->name;
        }
    }
    return $term_options;
}

/***********************************************************************************
 * Pages Custom Fields
 * ********************************************************************************/

/**
 * Front-page
 */
add_action(
    'cmb2_admin_init',
    function () {
        // Start with an underscore to hide fields from custom fields list
        $prefix = '_deliverymarinaflorescombr_frontpage_';

        /**
         * Hero
         */
        $cmb_hero = new_cmb2_box(
            array(
                'id'            => '_deliverymarinaflorescombr_frontpage_hero_id',
                'title'         => __('Hero', 'deliverymarinaflorescombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );
                
        //Hero Group
        $hero_id = $cmb_hero->add_field(
            array(
                'id'          => $prefix . 'hero_slides',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'   =>__('Slide {#}', 'deliverymarinaflorescombr'),
                    'add_button'   =>__('Add Another Slide', 'deliverymarinaflorescombr'),
                    'remove_button' =>__('Remove Slide', 'deliverymarinaflorescombr'),
                    'sortable'      => true, // beta
                ),
            )
        );

        //Background Color
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Background Color', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => 'bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#c12731'),
                        ),
                    ),
                ),
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Landscape)', 'deliverymarinaflorescombr'),
                'description' => '',
                'id'          => 'bkg_image_landscape',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'deliverymarinaflorescombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Image
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'        => __('Background Image (Portrait)', 'deliverymarinaflorescombr'),
                'description' => '',
                'id'          => 'bkg_image_portrait',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'deliverymarinaflorescombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Hero Title
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Title', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => 'title',
                'type'       => 'textarea_code',
            )
        );

        //Hero Text
        $cmb_hero->add_group_field(
            $hero_id,
            array(
                'name'       => __('Text', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => 'text',
                'type'       => 'textarea_code',
            )
        );

        /******
         * Steps
         ******/
        $cmb_steps = new_cmb2_box(
            array(
                'id'            => '_deliverymarinaflorescombr_frontpage_steps_id',
                'title'         => __('Steps', 'deliverymarinaflorescombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Steps Background Color
        $cmb_steps->add_field(
            array(
                'name'       => __('Background Color', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'alpha' => true, 
                            'palettes' => array('#ffffff', '#000000', '#c12731'),
                        )
                    ),
                ),
            )
        );
        
        //Steps Background Image
        $cmb_steps->add_field(
            array(
                'name'        => __('Background Image', 'deliverymarinaflorescombr'),
                'description' => '',
                'id'          => $prefix . 'steps_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'deliverymarinaflorescombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );

        //Steps Title
        $cmb_steps->add_field(
            array(
                'name'       => __('Title', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_title',
                'type'       => 'textarea_code',
            )
        );

        //Steps Text
        $cmb_steps->add_field(
            array(
                'name'       => __('Text', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_text',
                'type'       => 'textarea_code',
            )
        );

        //Steps Group
        $steps_id = $cmb_steps->add_field(
            array(
                'id'          => $prefix.'steps_items',
                'type'        => 'group',
                'description' => '',
                'options'     => array(
                    'group_title'  =>__('Step {#}', 'deliverymarinaflorescombr'),
                    'add_button' =>__('Add Another Step', 'deliverymarinaflorescombr'),
                    'remove_button'=>__('Remove Step', 'deliverymarinaflorescombr'),
                    'sortable'     => true, // beta
                ),
            )
        );

        //Step Item Icon
        $cmb_steps->add_group_field(
            $steps_id,
            array(
                'name'    => __('Icon', 'deliverymarinaflorescombr'),
                'desc'    => "",
                'id'      => 'icon',
                'type'    => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' => __('Add icon', 'deliverymarinaflorescombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                    //  'image/gif',
                        'image/jpeg',
                        'image/png',
                        'image/svg+xml',
                    ),
                ),
                'preview_size' => array(150, 150)
            )
        );

        //Step Item Title
        $cmb_steps->add_group_field(
            $steps_id,
            array(
                'name' => __('Title', 'deliverymarinaflorescombr'),
                'id'   => 'title',
                'type' => 'textarea_code',
            )
        );

        //Step Item Description
        $cmb_steps->add_group_field(
            $steps_id,
            array(
            'name' => __('Description', 'deliverymarinaflorescombr'),
            'id'   => 'desc',
            'description' => '',
            'type' => 'textarea_code',
            )
        );

        //Steps Note
        $cmb_steps->add_field(
            array(
                'name'       => __('Note', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_note',
                'type'       => 'textarea_code',
            )
        );

        //Steps Order Btn Text
        $cmb_steps->add_field(
            array(
                'name'       => __('Button Text', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_btn_text',
                'type'       => 'textarea_code',
            )
        );
        
        //Steps Order Btn URL
        $cmb_steps->add_field(
            array(
                'name'       => __('Button URL', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'steps_btn_url',
                'type'       => 'text',
            )
        );

        /**
         * Footer
         */
        $cmb_footer = new_cmb2_box(
            array(
                'id'            => '_deliverymarinaflorescombr_frontpage_footer_id',
                'title'         => __('Footer', 'deliverymarinaflorescombr'),
                'object_types'  => array('page'), // post type
                'show_on' => array('key' => 'slug', 'value' => 'front-page'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true, // Show field names on the left
            )
        );

        //Footer Background Color
        $cmb_footer->add_field(
            array(
                'name'       => __('Background Color', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_bkg_color',
                'type'       => 'colorpicker',
                'attributes' => array(
                    'data-colorpicker' => json_encode(
                        array(
                            'palettes' => array('#ffffff', '#000000', '#c12731'),
                        )
                    ),
                ),
            )
        );
        
        //Footer Background Image
        $cmb_footer->add_field(
            array(
                'name'        => __('Background Image', 'deliverymarinaflorescombr'),
                'description' => '',
                'id'          => $prefix . 'footer_bkg_image',
                'type'        => 'file',
                // Optional:
                'options' => array(
                    'url' => false, // Hide the text input for the url
                ),
                'text'    => array(
                    'add_upload_file_text' =>__('Add Image', 'deliverymarinaflorescombr'),
                ),
                // query_args are passed to wp.media's library query.
                'query_args' => array(
                    'type' => array(
                        //'image/gif',
                        'image/jpeg',
                        'image/png',
                    ),
                ),
                'preview_size' => array(320, 180)
            )
        );
        
        //Footer Title
        $cmb_footer->add_field(
            array(
                'name'       => __('Title', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_title',
                'type'       => 'textarea_code',
            )
        );

        //Footer Address
        $cmb_footer->add_field(
        array(
            'name'       => __('Address', 'deliverymarinaflorescombr'),
            'desc'       => '',
            'id'         => $prefix . 'footer_address',
            'type'       => 'textarea_code',
        )
        );

        //Footer Copyright
        $cmb_footer->add_field(
            array(
                'name'       => __('Text', 'deliverymarinaflorescombr'),
                'desc'       => '',
                'id'         => $prefix . 'footer_text',
                'type'       => 'textarea_code',
            )
        );
    }
);
